import { Component, OnInit, EventEmitter } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController, ModalController, Platform } from 'ionic-angular';
import { Constants } from '../../app/constants';
import { FormBuilder,	FormGroup, Validators } from '@angular/forms';
import { DatePicker } from '@ionic-native/date-picker';
import {MaskMoneyUtil} from "../../utilitarios/maskMoney";

//UTILITARIOS
import { PasswordValidation } from '../../utilitarios/password-validation';

//ENTITYS
import { UsuarioEntity } from './../../model/usuario-entity';
import { UsuarioDetalheEntity } from './../../model/usuario-detalhe-entity';

//PAGES
import { PrincipalPage } from '../principal/principal';
import { DetalheVagaPage } from './../detalhe-vaga/detalhe-vaga';

// SERVICES
import { LanguageTranslateService } from '../../providers/language-translate-service';
import { UsuarioService } from '../../providers/usuario-service';
import { LoginService } from '../../providers/login-service';
import { EstadosService } from '../../providers/estados-service';
import { CidadesService } from '../../providers/cidades-service';

// @IonicPage()
@Component({
  selector: 'page-meus-dados',
  templateUrl: 'meus-dados.html',
})
export class MeusDadosPage implements OnInit {
  public userChangeEvent = new EventEmitter();

  public dadosUsuarioForm: FormGroup;
  private usuarioDetalheEntity: UsuarioDetalheEntity;
  private usuarioEntity: UsuarioEntity;
  private loading = null;
  private isReadOnly: boolean;
  private dadosUsuarioFormat: any;
  private meusDadosFormat: any;
  private dadosLogin: any = {};
  public dataNascimento: string;
  public dataNascimentoFilhoCacula: string;

  public languageDictionary: any;

  private idUsuario: any;
  private nomePessoa: string;
  private isCadastroCompletoVaga: any;
  private isCadastroCompletoServico: any;
  public escondeIdade: boolean = true;

  public salario: any;
  private estados = [];
  private cidades = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              private usuarioService: UsuarioService,
              private formBuilder: FormBuilder,
              private toastCtrl: ToastController,
              public modalCtrl: ModalController,
              private maskMoney: MaskMoneyUtil,
              private loginService: LoginService,
              private estadosService: EstadosService,
              private cidadesService: CidadesService,
              private datePicker: DatePicker,
              public platform: Platform,
              private languageTranslateService: LanguageTranslateService) {

    this.usuarioDetalheEntity = new UsuarioDetalheEntity();
    this.usuarioEntity = new UsuarioEntity();
    this.idUsuario = localStorage.getItem(Constants.ID_USUARIO);
  }

  ngOnInit() {
    this.getTraducao();

    this.dadosUsuarioForm = this.formBuilder.group({
      'nomePessoa': ['', [Validators.required, Validators.maxLength(100)]],
      'email': ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      'genero': [''],
      'dataNascimento': [''],
      'dataNascimentoFilhoCacula': [''],
      'disponibilidadeMudanca': [''],
      'disponibilidadeHoraExtra': [''],
      'possuiFilhos': [''],
      'idadeFilhoCacula': [''],
      'nacionalidade': ['', Validators.maxLength(100)],
      'experienciaProfissional': ['', Validators.maxLength(500)],
      'grauEntendimento': [''],
      'grauFala': [''],
      'grauEscrita': [''],
      'salario': [''],
      'telefonePessoa': ['', [Validators.required, Validators.maxLength(50)]],
      'telefonePessoa2': ['', Validators.maxLength(50)],
      'endereco': ['', Validators.maxLength(300)],
      'cep': [''],
      'nomeResidencia': [''],
      'idEstado': [''],
      'idCidade': [''],
      'senhaUsuario': [''],
      'confirmSenha': ['']
    }, {
        validator: PasswordValidation.MatchPassword
      }
    );
    if(!localStorage.getItem(Constants.TOKEN_USUARIO)){
      this.isReadOnly = false;
      this.dadosUsuarioForm.get('senhaUsuario').setValidators([Validators.required]);      
    } else if(localStorage.getItem(Constants.TOKEN_USUARIO)) {
      this.isReadOnly = true;
      this.dadosUsuarioForm.get('genero').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('dataNascimento').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('disponibilidadeMudanca').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('disponibilidadeHoraExtra').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('nacionalidade').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('experienciaProfissional').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('grauEntendimento').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('grauFala').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('grauEscrita').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('endereco').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('cep').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('nomeResidencia').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('idEstado').setValidators([Validators.required]);
      this.dadosUsuarioForm.get('idCidade').setValidators([Validators.required]);
    }

    // this.dadosUsuarioForm.controls.dataNascimento.disable();
    // this.dadosUsuarioForm.controls.dataNascimentoFilhoCacula.disable();

    this.estadosService
      .getEstados()
      .subscribe(dados => {
      this.estados = dados;
    });

  }


  showIdadeFilhoCacula(event) {
    if(event == true || event == 'true') {
      this.escondeIdade = false;
    } else {
    this.escondeIdade = true;
    this.usuarioDetalheEntity.dataNascimentoFilhoCacula = null;
    // this.usuarioDetalheEntity.idadeFilhoCacula = null;
    }
  }

  ionViewDidLoad() {
    this.usuarioDetalheEntity.possuiFilhos = false;
  }

  getTraducao() {
    try {

      this.languageTranslateService
      .getTranslate()
      .subscribe(dados => {
        this.languageDictionary = dados;
        if(localStorage.getItem(Constants.TOKEN_USUARIO)) {
          this.callGetDadosUsuario();
        }
      });
    }
    catch (err){
      if(err instanceof RangeError){
        console.log('out of range');
      }
      console.log(err);
    }
  }

  public selecionaDataNasc() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      okText: 'OK',
      cancelText: 'Cancelar',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    })
    .then(dataNascimento => {
      this.dataNascimento = dataNascimento.toISOString();
    }, (err) => {
    });
  }

  public selecionaDataNascFilhoCacula() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      okText: 'OK',
      cancelText: 'Cancelar',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    })
    .then(dataNascimentoFilhoCacula => {
      this.dataNascimentoFilhoCacula = dataNascimentoFilhoCacula.toISOString();
    }, (err) => {
    });
  }

  getValorSalario(v) {
    this.salario = this.maskMoney.maskConvert(v);
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: this.languageDictionary.TOAST_CADASTRO_ATUALIZADO,
      duration: 3000,
      position: 'bottom',
      cssClass: "toast-success"
    });

    toast.onDidDismiss(() => {
    });

    toast.present();
  }

  submeterDadosUsuario() {
    try {

      this.dadosUsuarioFormat = this.dadosUsuarioForm.value;

      let dataNascimento = new Date(this.dataNascimento);
      this.dadosUsuarioFormat.dataNascimento = dataNascimento;
      
      let dataNascimentoFilhoCacula = new Date(this.dataNascimentoFilhoCacula);
      this.dadosUsuarioFormat.dataNascimentoFilhoCacula = dataNascimentoFilhoCacula;

      if (this.dadosUsuarioForm.valid) {
        
        this.loading = this.loadingCtrl.create({
          content: this.languageDictionary.LOADING_TEXT,
        });
        this.loading.present();
        
        if(!localStorage.getItem(Constants.TOKEN_USUARIO)){
          this.cadastraUsuario();
        }
        else if(localStorage.getItem(Constants.TOKEN_USUARIO)) {
          this.editaUsuario();
        }
      } else {
        Object.keys(this.dadosUsuarioForm.controls).forEach(campo => {
          const controle = this.dadosUsuarioForm.get(campo);
          controle.markAsTouched();
        })
      }
    }
    catch (err){
      if(err instanceof RangeError){
        console.log('out of range');
      }
      console.log(err);
    }
  }

  login(email, senha) {
    this.dadosLogin.login = email;
    this.dadosLogin.senha = senha;
    this.loginService.login(this.dadosLogin)
        .then((usuarioEntityResult: UsuarioEntity) => {

          if(!localStorage.getItem(Constants.ID_VAGA_CANDIDATAR)){
            this.navCtrl.setRoot(PrincipalPage);
          }
          else if(localStorage.getItem(Constants.ID_VAGA_CANDIDATAR)) {
              this.navCtrl.setRoot(DetalheVagaPage, {idVaga: localStorage.getItem(Constants.ID_VAGA_CANDIDATAR)});
          }

          this.loading.dismiss();
          this.navCtrl.setRoot(PrincipalPage);

        }, (err) => {
          this.loading.dismiss();
          err.message = err.message ? err.message : this.languageDictionary.LABEL_FALHA_CONEXAO_SERVIDOR;
          this.alertCtrl.create({
            subTitle: err.message,
            buttons: ['OK']
          }).present();
        });
  }

  cadastraUsuario() {
    this.meusDadosFormat = this.dadosUsuarioForm;
    this.meusDadosFormat.value.idiomaUsuario = localStorage.getItem(Constants.IDIOMA_USUARIO);
    this.meusDadosFormat.value.idiomaUsuario = localStorage.getItem(Constants.IDIOMA_USUARIO) == 'pt-br' ? 'PORTUGUES' : 'INGLES';

    this.usuarioService
    .adicionaUsuarioBasico(this.meusDadosFormat.value)
    .then((usuarioEntityResult: UsuarioEntity) => {

      this.isCadastroCompletoVaga = usuarioEntityResult.isCadastroCompletoVaga;
      localStorage.setItem(Constants.IS_CADASTRO_COMPLETO_VAGA, this.isCadastroCompletoVaga);
      this.isCadastroCompletoServico = usuarioEntityResult.isCadastroCompletoServico;
      localStorage.setItem(Constants.IS_CADASTRO_COMPLETO_SERVICO, this.isCadastroCompletoServico);

      this.login(this.meusDadosFormat.value.email, this.meusDadosFormat.value.senhaUsuario);

    }, (err) => {
      this.loading.dismiss();
      this.alertCtrl.create({
        subTitle: err.message,
        buttons: ['OK']
      }).present();
    });

  }

  editaUsuario() {
    // this.dadosUsuarioFormat = this.dadosUsuarioForm.value;

    console.log(this.dadosUsuarioFormat);

    if(this.dadosUsuarioFormat.salario) {
      for(let i=0; i <= this.dadosUsuarioFormat.salario.length; i++) {
        this.dadosUsuarioFormat.salario = this.dadosUsuarioForm.value.salario.replace(",", "");
      }
    }

    // let dataNascimento = new Date(this.dataNascimento);

    // console.log(dataNascimento);
    // console.log(this.dataNascimento);
    // console.log(this.dadosUsuarioFormat.dataNascimento);

    // this.dadosUsuarioFormat.dataNascimento = dataNascimento;

    // let dataNascimentoFilhoCacula = new Date(this.dataNascimentoFilhoCacula);
    // this.dadosUsuarioFormat.dataNascimentoFilhoCacula = dataNascimentoFilhoCacula;

    // console.log(this.dadosUsuarioFormat);

    this.usuarioService.atualizaUsuario(this.dadosUsuarioFormat)
    .then((usuarioDetalheEntityResult: UsuarioDetalheEntity) => {
      this.nomePessoa = usuarioDetalheEntityResult.nomePessoa;

      this.isCadastroCompletoVaga = usuarioDetalheEntityResult.isCadastroCompletoVaga;
      localStorage.setItem(Constants.IS_CADASTRO_COMPLETO_VAGA, this.isCadastroCompletoVaga);
      this.isCadastroCompletoServico = usuarioDetalheEntityResult.isCadastroCompletoServico;
      localStorage.setItem(Constants.IS_CADASTRO_COMPLETO_SERVICO, this.isCadastroCompletoServico);

      this.loading.dismiss();
      this.presentToast();

      
      setTimeout(() => {
        this.navCtrl.pop();
      }, 2000);
    }, (err) => {
      this.loading.dismiss();
      this.alertCtrl.create({
        subTitle: err.message,
        buttons: ['OK']
      }).present();
    });

  }

  callGetDadosUsuario() {
    try {
      this.loading = this.loadingCtrl.create({
        content: this.languageDictionary.LOADING_TEXT,
      });
      this.loading.present();

      this.usuarioService
        .getDadosUsuario()
        .then((dadosUsuarioDetalheResult) => {
          this.usuarioDetalheEntity = dadosUsuarioDetalheResult;
          this.dataNascimento = this.usuarioDetalheEntity.dataNascimento ? new Date(this.usuarioDetalheEntity.dataNascimento).toJSON().split('T')[0] : null;
          this.dataNascimentoFilhoCacula = this.usuarioDetalheEntity.dataNascimentoFilhoCacula ? new Date(this.usuarioDetalheEntity.dataNascimentoFilhoCacula).toJSON().split('T')[0] : null;
          
          //para testes no browser
          if (!this.platform.is('cordova')) {
            this.dataNascimento = new Date().toISOString();
          }
          if (!this.platform.is('cordova')) {
            this.dataNascimentoFilhoCacula = new Date().toISOString();
          }
          //para testes no browser - fim

          if(this.usuarioDetalheEntity.possuiFilhos) {
            this.escondeIdade = false;
          } else {
            this.escondeIdade = true;
          }

          if(this.usuarioDetalheEntity.salario) {
            let salario: any = this.usuarioDetalheEntity.salario;
            salario = salario.toString().split( /(?=(?:\d{3})+(?:\.|$))/g ).join( "," );
            this.salario = salario;
          }

          if(this.usuarioDetalheEntity.idEstado != null) {
            this.getCidadesByEstadoUsuario(this.usuarioDetalheEntity.idEstado, null);
          } else {
            this.loading.dismiss();
          }

        })
        .catch(err => {
          this.loading.dismiss();
          this.alertCtrl.create({
            subTitle: err.message,
            buttons: ['OK']
          }).present();
        });
    }catch (err){
      if(err instanceof RangeError){
      }
      console.log(err);
    }
  }

  getCidadesByEstadoUsuario(idEstado, botaoClicado) {
    try {
      if(botaoClicado) {
        this.loading = this.loadingCtrl.create({
          content: this.languageDictionary.LOADING_CIDADES,
        });
        this.loading.present();
      }

      this.cidadesService
        .getCidades(idEstado)
        .then((listCidadesResult) => {
          this.cidades = listCidadesResult;
          this.loading.dismiss();
        })
        .catch(err => {
          this.loading.dismiss();
          this.alertCtrl.create({
            subTitle: err.message,
            buttons: ['OK']
          }).present();
        });
    }catch (err){
      if(err instanceof RangeError){
      }
      console.log(err);
    }
  }

  onlyNumberKey(event: any) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

}
