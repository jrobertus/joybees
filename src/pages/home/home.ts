import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { Constants } from '../../app/constants';
import { Facebook } from '@ionic-native/facebook';

//PAGES
import { LoginPage } from '../login/login';
import { MeusDadosPage } from '../meus-dados/meus-dados';
import { ModalTermosPage } from '../modal-termos/modal-termos';
import { PrincipalPage } from '../principal/principal';
import { DetalheVagaPage } from './../detalhe-vaga/detalhe-vaga';
import { VagasEmDestaquePage } from './../vagas-em-destaque/vagas-em-destaque';
import { NovoOrcamentoPage } from '../novo-orcamento/novo-orcamento';
import { ModalPoliticaPrivacidadePage } from '../modal-politica-privacidade/modal-politica-privacidade';

//SERVICES
import { LoginService } from '../../providers/login-service';
import { LanguageTranslateService } from '../../providers/language-translate-service';

//ENTITYS
import { UsuarioEntity } from '../../model/usuario-entity';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  segment: string = "vagasDestaque"; // default button
  private usuarioEntity: UsuarioEntity;

  selectedLanguage: any;
  public loginPage: boolean;
  public languageDictionary: any;
  isLoggedIn: boolean = false;
  users: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private menu : MenuController,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              public fb: Facebook,
              private loginService: LoginService,
              private languageTranslateService: LanguageTranslateService,
              public modalCtrl: ModalController) {

    this.loginPage = navParams.get('loginPage');
    this.usuarioEntity = new UsuarioEntity();

    if(localStorage.getItem(Constants.ID_USUARIO)) {
      fb.getLoginStatus()
      .then(res => {
        if(res.status === "connect") {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log(e));
    }

  }

  ngOnInit() {
    this.getTraducao();
    if(this.loginPage == true) {
      this.segment = "login";
    }
  }

  ionViewDidLoad() {
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }

  getTraducao() {
    try {

      this.languageTranslateService
      .getTranslate()
      .subscribe(dados => {
        this.languageDictionary = dados;
      });
    }
    catch (err){
      if(err instanceof RangeError){
        console.log('out of range');
      }
      console.log(err);
    }
  }

  goLogin() {
    this.navCtrl.push(LoginPage);
  }

  goMeusDados() {
    this.navCtrl.push(MeusDadosPage);
  }

  goVagasEmDestaque() {
    this.navCtrl.push(VagasEmDestaquePage);
  }

  goFornecedoresEmDestaque() {
    this.navCtrl.push(NovoOrcamentoPage);
  }

  openTermos(){
    if(localStorage.getItem(Constants.IDIOMA_USUARIO) == 'pt-br') {
      window.open('http://joybees.com/br/termos-de-uso', '_system', 'location=yes');
    } else if (localStorage.getItem(Constants.IDIOMA_USUARIO) == 'en') {
      window.open('http://joybees.com/en/terms', '_system', 'location=yes');
    }
  }

  openPolitica(){
    if(localStorage.getItem(Constants.IDIOMA_USUARIO) == 'pt-br') {
      window.open('http://joybees.com/br/politica-de-privacidade', '_system', 'location=yes');
    } else if (localStorage.getItem(Constants.IDIOMA_USUARIO) == 'en') {
      window.open('http://joybees.com/en/privacy', '_system', 'location=yes');
    }
  }

  doFbLogin(){
    // this.fb.login(['public_profile', 'user_friends', 'email'])
    this.fb.login(['public_profile', 'email'])
    .then(res => {
      if(res.status === "connected") {
        this.isLoggedIn = true;
        this.getUserDetail(res.authResponse.userID);
      } else {
        this.isLoggedIn = false;
      }
    })
    .catch(e => console.log('Error logging into Facebook', e));
  }

  getUserDetail(userid) {
    this.fb.api("/"+userid+"/?fields=id,email,name,picture,gender",["public_profile"])
      .then(res => {
        this.users = res;
        this.usuarioEntity.idUsuarioFacebook = this.users.id;
        this.usuarioEntity.nomePessoa = this.users.name;
        this.usuarioEntity.genero = this.users.gender;
        this.usuarioEntity.login = this.users.email;

        //localStorage.setItem(Constants.CAMERA_DATA, this.users.picture.data.url); // TESTAR  <===================

      this.callLoginFacebook(this.usuarioEntity);
    })
    .catch(e => {
      console.log(e);
    });
  }

  callLoginFacebook(usuarioEntity) {
    this.loginService.loginFacebook(usuarioEntity)
    .then((usuarioEntityResult: UsuarioEntity) => {

      if(!localStorage.getItem(Constants.ID_VAGA_CANDIDATAR)){
        this.navCtrl.setRoot(PrincipalPage);
      }
      else if(localStorage.getItem(Constants.ID_VAGA_CANDIDATAR)) {
          this.navCtrl.setRoot(DetalheVagaPage, {idVaga: localStorage.getItem(Constants.ID_VAGA_CANDIDATAR)});
      }

    }, (err) => {
      // this.loading.dismiss();
      this.alertCtrl.create({
        subTitle: err.message,
        buttons: ['OK']
      }).present();
    });
  }

}
